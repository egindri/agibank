package com.agibank.report.business;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.agibank.report.entity.Salesperson;
import com.agibank.report.model.Report;

@RunWith(SpringRunner.class)
public class SalespersonProcessorTest {

	private SalespersonProcessor salespersonProcessor = new SalespersonProcessor();
	
	
	@Test
	public void processRecordTest() {
		
		Report report = new Report();
		String name = "Test";
		String[] attributes = new String[] {null, null, name};
	
		salespersonProcessor.processRecord(attributes, report);
	
		List<Salesperson> salespeople = report.getSalespeople();
		Assert.assertTrue(salespeople.size() == 1);
		Assert.assertTrue(salespeople.contains(new Salesperson(name)));
	}
}

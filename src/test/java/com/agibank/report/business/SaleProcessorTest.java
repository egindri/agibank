package com.agibank.report.business;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.agibank.report.entity.Salesperson;
import com.agibank.report.model.Report;

@RunWith(SpringRunner.class)
public class SaleProcessorTest {

	SaleProcessor saleProcessor = new SaleProcessor();
	
	
	@Test
	public void processRecordTest() {
		
		Report report = new Report();
		String name = "Test";
		String[] attributes = new String[] {null, "1", "[1-1-5,2-2-2.50]", name};
		
		saleProcessor.processRecord(attributes, report);
		
		Assert.assertTrue(report.getMostExpensiveSale().sumTotal().equals(BigDecimal.TEN.setScale(2)));
		List<Salesperson> salespeople = report.getSalespeople();
		Assert.assertTrue(salespeople.size() == 1);
		Assert.assertTrue(salespeople.get(0).equals(new Salesperson(name)));
	}
}

package com.agibank.report.business;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.agibank.report.entity.Customer;
import com.agibank.report.model.Report;

@RunWith(SpringRunner.class)
public class CustomerProcessorTest {
	
	CustomerProcessor customerProcessor = new CustomerProcessor();
	
	
	@Test
	public void processRecordTest() {
		
		Report report = new Report();
		String cpf = "99999999999";
		String name = "Test";
		String[] attributes = new String[] {null, cpf, name};
	
		customerProcessor.processRecord(attributes, report);
	
		Set<Customer> customers = report.getCustomers();
		Assert.assertTrue(customers.size() == 1);
		Assert.assertTrue(customers.stream().anyMatch(p -> p.getCpf().equals(cpf) && p.getName().equals(name)));
	}
}

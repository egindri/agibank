package com.agibank.report.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DataReader extends BaseIO {
	
	private static final String FILE_FORMAT = ".dat";

	private static final Logger LOGGER = LoggerFactory.getLogger(DataReader.class);
	
	private static final Path IN_DIR = Paths.get(CURRENT_DIR + "/data/in/");

	
	public List<String> readLines() throws IOException {
		try (Stream<Path> paths = Files.walk(IN_DIR)) {
			
			return paths.filter(Files::isRegularFile)
						.filter(p -> isDat(p))
						.flatMap(p -> readLines(p))
						.filter(p -> p != null)
						.collect(Collectors.toList());
		}
	}
	
	private boolean isDat(Path p) {
		return p.toString().endsWith(FILE_FORMAT);
	}

	private Stream<String> readLines(Path path) {
		try {
			return Files.lines(path);
		} catch (IOException e) {
			LOGGER.error("Erro ao analisar arquivo!", e);
			return null;
		}
	}
}

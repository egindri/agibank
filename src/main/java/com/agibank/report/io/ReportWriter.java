package com.agibank.report.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportWriter extends BaseIO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportWriter.class);
	
	private static final Path OUT_DIR = Paths.get(CURRENT_DIR + "data/out/report.done.dat");
	
	
	public static void writeContent(String string) {
		try {
			Files.write(OUT_DIR, string.getBytes());
		} catch (IOException e) {
			LOGGER.error("Erro ao escrever arquivo!", e);
		}
	}
}

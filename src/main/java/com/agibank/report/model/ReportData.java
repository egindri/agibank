package com.agibank.report.model;

public class ReportData {
	
	private int clientQuantity;
	private int salespersonQuantity;
	private String mostExpensiveSaleId;
	private String worstSalespersonName;

	
	public ReportData(int clientQuantity, int salespersonQuantity, String mostExpensiveSaleId, String worstSalespersonName) {
		this.clientQuantity = clientQuantity;
		this.salespersonQuantity = salespersonQuantity;
		this.mostExpensiveSaleId = mostExpensiveSaleId;
		this.worstSalespersonName = worstSalespersonName;
	}


	public String toString() {
		return new StringBuilder().append("Quantidade de clientes no arquivo de entrada: " + clientQuantity + newLine())
			  					  .append("Quantidade de vendedores no arquivo de entrada: " + salespersonQuantity + newLine())
			  					  .append("ID da venda mais cara: " + mostExpensiveSaleId + newLine())
			  					  .append("Pior vendedor: " + worstSalespersonName)
								  .toString();
	}

	private String newLine() {
		return System.lineSeparator();
	}
}

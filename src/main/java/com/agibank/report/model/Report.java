package com.agibank.report.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.agibank.report.entity.Customer;
import com.agibank.report.entity.Sale;
import com.agibank.report.entity.Salesperson;

public class Report {
	
	private List<Salesperson> salespeople = new ArrayList<>();
	private Set<Customer> customers = new HashSet<>();
	private Sale mostExpensiveSale;
	
	
	public List<Salesperson> getSalespeople() {
		return salespeople;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public Sale getMostExpensiveSale() {
		return mostExpensiveSale;
	}

	public void calcMostExpensiveSale(Sale sale) {
		
		if (mostExpensiveSale == null || sale.sumTotal().compareTo(mostExpensiveSale.sumTotal()) > 0) {
			this.mostExpensiveSale = sale;
		}
	}
}
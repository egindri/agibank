package com.agibank.report.entity;

public class Person {
	
	private String name;
	
	
	public Person(String name) {
		this.name = name;
	}
	

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object salesperson) {
		return salesperson != null 
			&& (this == salesperson || getClass().equals(salesperson.getClass()) && name.equals(((Person) salesperson).getName())); 
	}
}

package com.agibank.report.entity;

import java.math.BigDecimal;
import java.util.List;

public class Sale {

	private String id;
	private List<SaleItem> saleItems;
	
	
	public Sale(String id, List<SaleItem> saleItems) {
		this.id = id;
		this.saleItems = saleItems;
	}
	
	
	public String getId() {
		return id;
	}
	
	public List<SaleItem> getSaleItems() {
		return saleItems;
	}
	
	public BigDecimal sumTotal() {
		return saleItems.stream()
						.map(s -> s.getItem().getPrice().multiply(new BigDecimal(s.getQuantity())))
						.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}
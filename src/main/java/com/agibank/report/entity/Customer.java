package com.agibank.report.entity;

public class Customer extends Person {

	private String cpf;
	
	
	public Customer(String cpf, String name) {
		super(name);
		this.cpf = cpf;
	}
	
	
	public String getCpf() {
		return cpf;
	}

	@Override
	public int hashCode() {
		return cpf.hashCode();
	}
	
	@Override
	public boolean equals(Object customer) {
		return customer != null 
			&& (this == customer || customer instanceof Customer && cpf.equals(((Customer) customer).getCpf()));
	}
}

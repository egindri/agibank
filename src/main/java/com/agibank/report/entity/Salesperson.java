package com.agibank.report.entity;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class Salesperson extends Person implements Comparable<Salesperson> {

	private Set<Sale> sales = new HashSet<>();

	
	public Salesperson(String name) {
		super(name);
	}
	
	
	public void addSale(Sale sale) {
		sales.add(sale);
	}

	@Override
	public int compareTo(Salesperson o) {
		return sumTotalSales().compareTo(o.sumTotalSales());
	}
	
	private BigDecimal sumTotalSales() {
		return sales.stream()
					.map(Sale::sumTotal)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}

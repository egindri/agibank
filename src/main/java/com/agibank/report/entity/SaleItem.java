package com.agibank.report.entity;

public class SaleItem {

	private Item item;
	private int quantity;
	
	
	public SaleItem(int quantity, Item item) {
		this.item = item;
		this.quantity = quantity;
	}
	
	
	public Item getItem() {
		return item;
	}
	
	public int getQuantity() {
		return quantity;
	}
}
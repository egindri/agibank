package com.agibank.report.business;

import org.springframework.stereotype.Component;

import com.agibank.report.entity.Customer;
import com.agibank.report.model.Report;

@Component
public class CustomerProcessor extends RecordProcessor {
	
	public void processRecord(String[] attributes, Report report) {
		
		String cpf = attributes[1];
		String name = attributes[2];
		
		Customer customer = new Customer(cpf, name);
		report.getCustomers().add(customer);
	}
}

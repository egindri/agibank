package com.agibank.report.business;

import org.springframework.stereotype.Component;

import com.agibank.report.entity.Salesperson;
import com.agibank.report.model.Report;

@Component
public class SalespersonProcessor extends RecordProcessor {

	public void processRecord(String[] attributes, Report report) {
		String name = attributes[2];
		Salesperson salesperson = new Salesperson(name);
		insertSalesperson(salesperson, report);
	}
}
package com.agibank.report.business;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agibank.report.entity.Salesperson;
import com.agibank.report.io.DataReader;
import com.agibank.report.io.ReportWriter;
import com.agibank.report.model.Report;
import com.agibank.report.model.ReportData;

@Component
public class ReportGenerator {
	
	private static final String SEPARATOR = "ç";

	@Autowired
	private RecordProcessorFactory recordProcessorFactory;

	@Autowired
	private DataReader dataReader;
	
	
	public void generate() throws IOException {
		List<String> records = dataReader.readLines();
		
		Report report = new Report();
		records.forEach(r -> processRecord(report, r));
		
		ReportData reportData = buildReport(report);
		
		ReportWriter.writeContent(reportData.toString());
	}

	private void processRecord(Report report, String r) {
		String[] attributes = r.split(SEPARATOR);
		recordProcessorFactory.getInstance(attributes).processRecord(attributes, report);
	}
	
	private ReportData buildReport(Report report) {
		
		int clientQuantity = report.getCustomers().size();
		List<Salesperson> salespeople = report.getSalespeople();
		String mostExpensiveSaleId = report.getMostExpensiveSale().getId();
		String worstSalespersonName = Collections.min(salespeople).getName();
		
		return new ReportData(clientQuantity, salespeople.size(), mostExpensiveSaleId, worstSalespersonName);
	}
}
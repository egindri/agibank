package com.agibank.report.business;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.agibank.report.entity.Item;
import com.agibank.report.entity.Sale;
import com.agibank.report.entity.SaleItem;
import com.agibank.report.entity.Salesperson;
import com.agibank.report.model.Report;

@Component
public class SaleProcessor extends RecordProcessor {

	private static final String SALE_SEPARATOR = ",";
	private static final String SALE_ITEM_SEPARATOR = "-";
	

	public void processRecord(String[] attributes, Report report) {
		String items = attributes[2];
		List<SaleItem> saleItems = processSaleItems(items);
		
		String id = attributes[1];
		Sale sale = new Sale(id, saleItems);
		
		report.calcMostExpensiveSale(sale);
		
		String salespersonName = attributes[3];
		Salesperson salesperson = new Salesperson(salespersonName);
		insertSalesperson(salesperson, report).addSale(sale);
	}

	private List<SaleItem> processSaleItems(String items) {
		String[] saleItems = items.replaceAll("\\[|\\]", "").split(SALE_SEPARATOR);
		return Stream.of(saleItems)
					 .map(i -> processSaleItem(i))
					 .collect(Collectors.toList());
	}

	private SaleItem processSaleItem(String item) {
		String[] saleItem = item.split(SALE_ITEM_SEPARATOR);
		Integer quantity = Integer.valueOf(saleItem[1]);
		return new SaleItem(quantity, new Item(new BigDecimal(saleItem[2])));
	}
}

package com.agibank.report.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agibank.report.exception.InvalidRecordTypeException;

@Component
public class RecordProcessorFactory {

	@Autowired
	private CustomerProcessor customerProcessor;
	
	@Autowired
	private SaleProcessor saleProcessor;
	
	@Autowired
	private SalespersonProcessor salespersonProcessor;
	
	
	public RecordProcessor getInstance(String[] attributes) {
		String formatId = attributes[0];
		
		switch (formatId) {
			case "001":
				return salespersonProcessor;
			case "002": 
				return customerProcessor;
			case "003": 
				return saleProcessor;
		}
		
		throw new InvalidRecordTypeException();
	}
}

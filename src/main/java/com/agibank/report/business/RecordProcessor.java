package com.agibank.report.business;

import java.util.List;

import com.agibank.report.entity.Salesperson;
import com.agibank.report.model.Report;

public abstract class RecordProcessor {
	
	public abstract void processRecord(String[] attributes, Report report);

	protected Salesperson insertSalesperson(Salesperson salesperson, Report report) {
		List<Salesperson> salespeople = report.getSalespeople();
		
		int index = salespeople.indexOf(salesperson);
		if (index == -1) {
			salespeople.add(salesperson);
			return salesperson;
		}
		
		return salespeople.get(index);
	}
}

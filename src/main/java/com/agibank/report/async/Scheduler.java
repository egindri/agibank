package com.agibank.report.async;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.agibank.report.business.ReportGenerator;

@Component
public class Scheduler {
	
	@Autowired
	private ReportGenerator reportGenerator;
	
	
	@Scheduled(fixedDelay = 1)
	public void generateReport() throws IOException {
		reportGenerator.generate();
	}
}